<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
Route::get('/', function () {
    return view('user.register');
});

Route::get('login','LoginController@index');
Route::post('/login-check', ['as' => 'login.post', 'uses' => 'LoginController@login_check']);

Route::post('signup/submit', ['as' => 'register.post', 'uses' => 'RegisterController@store']);

Route::get('setting','SettingController@index');
Route::post('setting/email','SettingController@save_email_config');
Route::post('setting/send_mail','SettingController@send_mail');
Route::post('setting/sendmail_store','SettingController@sendmail_store');

Route::get('setting/getUserEmail','SettingController@getUserEmail');




});

