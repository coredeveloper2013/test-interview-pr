
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vueResource from 'vue-resource'
Vue.use(vueResource)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('send-email', require('./components/SendEmailForm.vue'));

const app = new Vue({
    el: '#app',
    data: function () {
    	return {
    		showingModal: false,
            messages: [],
			successMessage: "mmmmmmmmmoooooooozzzzz",
    	}

    	},

    	mounted(){
    		console.log('Component Vue form')            
    	},
        created() {
          //   alert('Get emails');
            window.axios.get('./setting/getUserEmail').then(({ data }) => {
                this.messages = data;
                console.log(this.messages)
            });
        },
    	methods:{
    		addTask: function(){
    			alert(this.successMessage);
    		},
            fetchUsersInfo: function(){
                this.showingModal = false;
                window.axios.get('./setting/getUserEmail').then(({ data }) => {
                this.messages = data;
                location.reload(true)
                console.log(this.messages)
            });
            }
            
    	}
		
	
})

