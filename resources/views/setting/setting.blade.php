<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ url('/css/style.css') . '?v=' . time() }}" rel="stylesheet">

        <!-- Styles -->
        <style>
                  .modal-mask {
          position: fixed;
          z-index: 9998;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background-color: rgba(0, 0, 0, .5);
          display: table;
          transition: opacity .3s ease;
        }

        .modal-wrapper {
          display: table-cell;
          vertical-align: middle;
        }

        .modal-container {
          width: 500px;
          margin: 0px auto;
          padding: 20px 30px;
          background-color: #fff;
          border-radius: 2px;
          box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
          transition: all .3s ease;
          font-family: Helvetica, Arial, sans-serif;
        }

        .modal-header h3 {
          margin-top: 0;
          color: #42b983;
        }

        .modal-body {
          margin: 20px 0;
        }

        .modal-default-button {
          float: right;
        }

        .modal-enter {
          opacity: 0;
        }

        .modal-leave-active {
          opacity: 0;
        }

        .modal-enter .modal-container,
        .modal-leave-active .modal-container {
          -webkit-transform: scale(1.1);
          transform: scale(1.1);
        }
        .r-float{ float: right; }
        .err-body{
            background-color: #d89393;
            text-align: center;
        }
        .err-mgs{
            font-size: 15px;
            color:red;
        }
        table {
            background-color: #dcd4d4;
        }
      </style>
    </head>
    <body>
    <div class="flex-center position-ref full-height">

    <div class="content" id="app">

    <div class="main-container">
        <div class="container" >
            <div class="row">

                @if (isset($errors) and count($errors) > 0)
                    <div class="col-lg-12">
                        <div class="alert alert-danger">
                            <ul class="list list-check">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                @if(Session::has('mgs'))

                <div class=""><p class="success custom-smgs"><h3 style="color:green; text-indent: 20%;">{{ Session::get('mgs') }}{{ Session::put('mgs', '') }}</h3></p></div>
                @endif  

        

                <div class="col-md-6 page-content">
                    <div class="inner-box category-content">
                        <h2 class="title-2"><strong> <i class="icon-user-add"></i> {{ 'Email Setting' }}</strong> 
                            <button id="sendemail_btn" style="float: right;" class="btn btn-success btn-sm" @click="showingModal = true"> {{ 'Send Email' }} </button>
                            <!-- <button v-on:click="sendEmail()">Say hi</button> -->
                        </h2>
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <form class="form-horizontal" method="POST" action="{{ url('setting/email') }}">
                                    {!! csrf_field() !!}
                                    <fieldset>

                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('email_type')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label">{{ 'Email Engine' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <select autofocus="" id="email_type" name="email_type" class="form-control">
                                                        <option value="smpt">SMPT</option>
                                                        <option value="sendmail">SendMail</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('smtp_username')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label">{{ 'SMTP Username' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input name="smtp_username" placeholder="{{ 'SMTP Username' }}" class="form-control input-md" type="text"
                                                       value="{{ old('smtp_username') }}">
                                            </div>
                                        </div>

                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('smtp_password')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label" for="password">{{ 'SMTP Password' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input id="smtp_password" name="smtp_password" type="password" class="form-control"
                                                       placeholder="{{ 'SMTP Password' }}">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('smtp_server')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label" for="phone">{{ 'SMTP Server' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-phone"></i></span>
                                                    <input id="smtp_server" name="smtp_server" type="text" class="form-control" placeholder="{{ 'SMTP Server' }}" value="{{ old('smtp_server') }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('smtp_port')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label" for="phone">{{ 'SMTP Port' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-phone"></i></span>
                                                    <input id="smtp_port" name="smtp_port" type="text" class="form-control" placeholder="{{ 'SMTP Port' }}" value="{{ old('smtp_port') }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group required <?php echo (isset($errors) and $errors->has('smtp_security')) ? 'has-error' : ''; ?>">
                                            <label class="col-md-4 control-label" for="phone">{{ 'SMTP Security' }} <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-phone"></i></span>
                                                    <input id="smtp_security" name="smtp_security" type="text" class="form-control" placeholder="{{ 'SMTP Security' }}" value="{{ old('smtp_security') }}">
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <!-- Button  -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <button id="signup_btn" class="btn btn-success btn-lg"> {{ 'Save' }} </button>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 30px;"></div>

                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2>Users Info</h2>
                    <table class="table table-striped">
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message</th>
                      </tr>
                        <tr v-for="message in messages">
                            <td>@{{ message.name }}</td>
                            <td>@{{ message.email }}</td>
                            <td>@{{ message.message }}</td>
                        </tr>
                   
                    </table>
                </div>

                <div class="">
                        <send-email  v-if="showingModal" @close="showingModal = false"></send-email>
                </div>

                
                </div>

                </div>
              </div>

            </div>

        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
