@extends('layouts.master')
@section('maincontent')
	<div class="main-container">
		<div class="container">
			<div class="row">

				

				@if (isset($errors) and count($errors) > 0)
					<div class="col-lg-12">
						<div class="alert alert-danger">
							<ul class="list list-check">
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				@endif

				@if(Session::has('logmgs'))

                <div class="alert alert-danger"><p class="success custom-smgs">{{ Session::get('logmgs') }}{{ Session::put('logmgs', '') }}</p></div>
                @endif  
                @if(Session::has('login_errs'))
                <div class=""><p class="warning custom-warnmgs"><h3 style="color:red;">{{ Session::get('login_errs') }}{{ Session::put('login_errs', ' ') }} </h3></p></div>
                @endif 


				<div class="col-md-4">
					
					<div class="col-sm-12 login-box">
					<form id="loginForm" role="form" method="POST" action="{{ url('login-check') }}">
						{!! csrf_field() !!}
						<div class="panel panel-default">
							<div class="panel-intro text-center">
								<h2 class="logo-title">
									<strong><span class="logo-icon"></span>{{ 'Log in' }}</strong>
								</h2>
							</div>
							<div class="panel-body">
								<div class="form-group <?php echo ($errors->has('email')) ? 'has-error' : ''; ?>">
									<label for="email" class="control-label">{{ 'Email Address' }}:</label>
									<div class="input-icon"><i class="icon-user fa"></i>
										<input id="email" name="email" type="text" placeholder="{{ 'Email Address' }}" class="form-control email"
											   value="{{ (session('email')) ? session('email') : old('email') }}">
									</div>
								</div>
								<div class="form-group <?php echo ($errors->has('password')) ? 'has-error' : ''; ?>">
									<label for="password" class="control-label">{{ 'Password' }}:</label>
									<div class="input-icon"><i class="icon-lock fa"></i>
										<input id="password" name="password" type="password" class="form-control" placeholder="{{ 'Password' }}">
									</div>
								</div>
								<div class="form-group">
									<button id="loginBtn" class="btn btn-primary btn-block"> {{ 'Login' }} </button>
								</div>
							</div>
							
						</div>
					</form>

					<div class="login-box-btm text-center">
						<p> {{ 'Don\'t have an account?' }} <br>
							<a href="./"><strong>{{ 'Sign Up' }} !</strong> </a></p>
					</div>
				</div>

				</div>

				
			</div>
		</div>
	</div>
@endsection
