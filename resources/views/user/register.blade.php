@extends('layouts.master')
@section('maincontent')
	<div class="main-container">
		<div class="container">
			<div class="row">

				@if (isset($errors) and count($errors) > 0)
					<div class="col-lg-12">
						<div class="alert alert-danger" style="color: #da2065;font-size: 15px;font-weight: bold;">
							<ul class="list list-check">
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				@endif

				@if(Session::has('logmgs'))

                <div class="alert alert-danger"><p class="success custom-smgs">{{ Session::get('logmgs') }}{{ Session::put('logmgs', '') }}</p></div>
                @endif  
                @if(Session::has('login_errs'))
                <div class=""><p class="warning custom-warnmgs"><h3 style="color:red;">{{ Session::get('login_errs') }}{{ Session::put('login_errs', ' ') }} </h3></p></div>
                @endif 

		

				<div class="col-md-8 page-content">
					<div class="inner-box category-content">
						<h2 class="title-2"><strong> <i class="icon-user-add"></i> {{ 'Create your account, Its free' }}</strong></h2>
						<div class="row">
							
							<div class="col-sm-12">
								<form id="signup_form" class="form-horizontal" method="POST" action="{{ url('signup/submit') }}">
									{!! csrf_field() !!}
									<fieldset>

										<!-- First Name -->
										<div class="form-group required <?php echo (isset($errors) and $errors->has('name')) ? 'has-error' : ''; ?>">
											<label class="col-md-4 control-label">{{ 'Name' }} <sup>*</sup></label>
											<div class="col-md-6">
												<input name="name" placeholder="{{ 'Name' }}" class="form-control input-md" type="text"
													   value="{{ old('name') }}">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('email')) ? 'has-error' : ''; ?>">
											<label class="col-md-4 control-label" for="email">{{ 'Enter Email' }} <sup>*</sup></label>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-mail"></i></span>
													<input id="email" name="email" type="text" class="form-control" placeholder="{{ 'Enter Email' }}"
														   value="{{ old('email') }}">
														   
												</div>
												<span class="help-block" id="available_check"></span>
											</div>
										</div>
										
										
										<div class="form-group required <?php echo (isset($errors) and $errors->has('mobile_no')) ? 'has-error' : ''; ?>">
											<label class="col-md-4 control-label" for="phone">{{ 'Phone no' }} <sup>*</sup></label>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-phone"></i></span>
													<input id="phone" name="mobile_no" type="text" class="form-control" placeholder="{{ 'Phone no' }}" value="{{ old('mobile_no') }}" maxlength="14">
												</div>
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('password')) ? 'has-error' : ''; ?>">
											<label class="col-md-4 control-label" for="password">{{ 'Password' }} <sup>*</sup></label>
											<div class="col-md-6">
												<input id="password" name="password" type="password" class="form-control"
													   placeholder="{{ 'Password' }}">
											</div>
										</div>

										
										<!-- Button  -->
										<div class="form-group">
											<label class="col-md-4 control-label"></label>
											<div class="col-md-8">
												<button id="signup_btn" class="btn btn-success btn-lg" style="margin-left: 115px;"> {{ 'Register' }} </button>
											</div>
										</div>

										<div style="margin-bottom: 30px;"></div>

									</fieldset>
								</form>
							</div>
							<div class="login-box-btm text-center">
						<h5> {{ 'Have an account?' }} </h5>
							<p><a href="./login"><strong>{{ 'Sign In' }} !</strong> </a></p>
					</div>
						</div>
					</div>
				</div>

				
			</div>
		</div>
	</div>
@endsection
