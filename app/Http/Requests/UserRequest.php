<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:5',
            
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Please enter a Name!',
            'email.required' => 'Please enter your e-mail address!',
            'password.required' => 'Passowrd Need Atleast 6 Charecter',
        ];
    }
}
