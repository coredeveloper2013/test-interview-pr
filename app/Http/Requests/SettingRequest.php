<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_type' => 'required',
            'smtp_username' => 'required',
            'smtp_password' => 'required',
            'smtp_server' => 'required',
            'smtp_port' => 'required',
            'smtp_security' => 'required',            
        ];
    }

    public function messages(){
        return [
            'email_type.required' => 'Please Email Engine type!',
            'smtp_username.required' => 'Please enter your SMPT Username!',
            'smtp_password.required' => 'Please enter your SMPT Password!',
            'smtp_server.required' => 'Please enter your SMPT Servername!',
            'smtp_port.required' => 'Please enter your SMPT Port!',
            'smtp_security.required' => 'Please enter your SMPT Security!',
        ];
    }
}
