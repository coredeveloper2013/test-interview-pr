<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Session;
use Validator;
use Redirect;

class LoginController extends Controller
{

  public function index()
    {
        $data = array();
        $data['title'] = 'User Login';
        return view('user.login', $data);
    }

    public function login_check(Request $request){
        $email = $request->email;
        $password = $request->password;
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required'
        ]);

      //  dd($validator->fails());

        if(!$validator->fails()){
            $user_info = DB::table('users AS u')
                    ->select('u.id', 'u.name', 'u.password')
                    ->where('u.email', $email)
                    ->first();
          
        if(empty($user_info)){
        
            Session::put('login_errs', 'Email Doesn\'t match !');
            return redirect('/');
        }
     
        if(Hash::check($password, $user_info->password)){
            Session::put('user_id', $user_info->id);
            Session::put('name', $user_info->name);
            Session::put('logmgs','Login Successfully !');
            return redirect('/setting');
        }else{
            Session::put('login_errs', 'Password Doesn\'t match !');
            return redirect('/');
        }
        }else{
           return Redirect::back()->withErrors($validator)->withInput();
        }
        
    }


}
