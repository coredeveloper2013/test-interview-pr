<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SettingRequest;
use App\Models\SettingModel;
use App\Models\EmailModel;
use Illuminate\Support\Facades\Mail;
use Session;
use Redirect;
use DB;

class SettingController extends Controller
{
    
    public function index()
    {        
        return view('setting.setting');
    }

    public function getUserEmail(){
      $listUsers = EmailModel::select('id', 'name', 'email', 'message')
            ->orderBy('id', 'desc')
            ->get();
        return  json_encode($listUsers);
    }


    public function save_email_config(SettingRequest $request)
    {

    	$emailsetting = new SettingModel();
        $emailsetting->email_type = $request->email_type;
        $emailsetting->smtp_username = $request->smtp_username;
        $emailsetting->smtp_password = $request->smtp_password;
        $emailsetting->smtp_server = $request->smtp_server;
        $emailsetting->smtp_port = $request->smtp_port;
        $emailsetting->smtp_security = $request->smtp_security;
        $emailsetting->save();
        Session::put('mgs', 'Email Configuration save Successfully !');
        return Redirect::back();

    }

    public function sendmail_store(Request $request){
    
    //  dd('hii');
      $email = new EmailModel();
        $email->name = $request->name;
        $email->email = $request->email;
        $email->message = $request->message;
        $email->save();

      $emailConfig = DB::table('settings')
                 ->select('*')
                 ->first(); 


      //  dd($emailConfig);         
          
        $mail = new \PHPMailer(true);


    //    $mail->IsSMTP();
        // $mail->SMTPAuth   = false; 
        // $mail->SMTPSecure = 'tls';
        // $mail->Host       = 'mail.equidesk.com';
        // $mail->Port       = 587;
        // $mail->Username = 'test007@equidesk.com';
        // $mail->Password = '!@#123!@#';


        // $mail->SMTPDebug  = 2; 
        $mail->IsSMTP(); 
        $mail->SMTPAuth   = true; 
        $mail->SMTPSecure = $emailConfig->smtp_security;
        $mail->Host       = $emailConfig->smtp_server;
        $mail->Port       = $emailConfig->smtp_port;
        $mail->Username = $emailConfig->smtp_username;
        $mail->Password = $emailConfig->smtp_password;

        $mail->SetFrom($emailConfig->smtp_username, 'Equidesk');
        $mail->Subject = "Email Sent Test Project";
        $mail->Body    = $request->message;;
        $mail->AddAddress($request->email, $request->name);
       // dd($mail->Send());
        if ($mail->Send()) {
            return(['mgs' => 'Email Store Successfully !']);
        } else {
            echo "Mailer Error: " . $mail->ErrorInfo;
          //  return 'Failed to Send Email';
        }



    }



        public function send_mail(Request $request) {

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['message'] = $request->message;
        $emailConfig = DB::table('settings')
                 ->select('*')
                 ->first();	


         Mail::send('emails.send_mail', $data, function($message) use($data){
		            $message->from('test007@equidesk.com', 'Equidesk');
                    $message->to($data['email'], $data['name']);
                    $message->subject($data['message']);
                    
        });        

    }



}
